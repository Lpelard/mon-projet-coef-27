#######################

import typing
from easygui import *

#######################
#ici j'importe pour ce dont j'aurais besoins

#je cree une fonction pour definir le pseudo du joueur
nom = input("Comment veux tu t'appeler? ")
def ton_pseudo(pseudo:str):
    pseudo1 = pseudo[0].upper()
    pseudo1 += pseudo[1:].lower()
    return pseudo1
pseudo = f"{ton_pseudo(nom)}"

#je demande a la fonction de demander le nom du joueur

#je vais cree une interface pour mon escapegame
title = pseudo + " voici mon escapegame!"
message="A toi de trouver le suspect, l'arme, et la pièce du meurtre. Bonne chance!\n\nJe te conseille de te munir d'un crayon et de papier afin de bien te souvenir de tout car tu ne pourras pas revenir en arrière."
msgbox(message,title,"Click quand tu es pret")
#jai cree la premiere fenetre de mon jeu.

#je commence la premiere partie de mon scape game qui en 3 partie(personage, arme, piece)
choix= 'Oui', 'Non'
if ynbox("Es-tu vraiment pret?","",choix):#je cree la box qui permet de stopper l'escapegame

    im="5_suspect.gif.gif"#ici l'image est defini
    buttonbox("Voici les 5 suspects \nRetiens-les bien!\n(Tu peux prendre des notes sur un papier.)",image = im,choices=["Ok"])
    #je cree la premiere partie de la premiere partie :) avec l'image

    a='Le suspect est-il le plus petit?'
    b='Le suspect a-t-il un pantalon noir?'
    c='Le suspect a-t-il une cravate?'
    choix =a,b,c
    #je defini les choix de la premiere partie
    
    i=0
    #i est une variable pour compter

    msg1="La reponse est:\nNON"
    msg2="La reponse est:\nOUI"
    #je defini les 2 messages de cette parties

    while i<2:#je cree une boucle

        rep = buttonbox("Quelles questions veux-tu poser?(2 chances en tout)","Première partie",image=im,choices= choix)
        #s'agit de l'interface de question

        if rep ==a:
            msgbox(msg1,"","Il te reste peut-être une chance?")
            i=i+1
        elif rep == b:
            msgbox(msg1,"","Il te reste peut-être une chance?")
            i=i+1
        elif rep == c:
            msgbox(msg2,"","Il te reste peut-être une chance?")
            i=i+1
            #jai cree les 3 possibilite de question

    rep2 = buttonbox("Malheureusement tu n'as plus de chance.\nA ton avis qui est le suspect?","Première partie",image=im,choices=["1","2","3","4","5"])
    #je cree la reponse

    if rep2 == "4":
        msgbox("Bien joué!\nLe première indice: UB","Félicitation!","Passer à la deuxième partie")
    else:
        msgbox("Dommage c'était le n°4 !","Mince!","Passer à la deuxième partie")
    #je cree les 2 possibilite de reponse suite au boutton de reponse



    #on passe a la deuxieme partie

    msgbox("La prochaine interface affichera la liste des 5 armes suspectes \nRetiens-les bien!\n(Tu peux les noter sur un papier.)")
    #je cree la premiere partie de ma deuxieme partie

    liste=("1:Couteau","2:Revolver","3:Cross_de_rink_hockey","4:Bombe_chimique","5:Medicaments")
    msgbox(liste)
    #je cree la liste des armes et je fais apparetre la liste dans une interface

    choix2="La victime saigne-t-elle?","La victime a-t-elle subit un traumatisme crânien?","A-t-on retrouvé l'arme du crime dans le sasng de la victime?"
    #je cree les choix pour la troisieme partie

    k=0#k sera un variable pour compter

    msgA="La reponse est:\nOUI"
    msgB="La reponse est:\nNON"
    msgC="La victime est morte sur le coup"
    #je defini les message qui seront afficher

    while k<2:#je recommence une autre boucle pour que 2 question soit possé

        rep3=buttonbox("Quelles questions veux-tu poser?(2 chances en tout)","Deuxième partie",choices=choix2)
        #je cree le boutton pour les question defini dans choix2

        if rep3 == choix2[0]:
            msgbox(msgB,"","Il te reste peut-être une chance?")
            k=k+1
        elif rep3 == choix2[1]:
            msgbox(msgA,"","Il te reste peut-être une chance?")
            k=k+1
        elif rep3 == choix2[2]:
            msgbox(msgC,"","Il te reste peut-être une chance?")
            k=k+1
        #je cree les 3 reponse qui vont pouvoir s'afficher

    rep4 = buttonbox("Malheureusement tu n'as plus de chance. \nA ton avis quelle est l'arme du suspect","Deuxième partie",[liste[0],liste[1],liste[2],liste[3],liste[4]])
    #je cree la verification de la deuxieme partie

    if rep4 == liste[2]:
        msgbox("Bien joué!\nLe deusième indice: UN","Félicitation","Passer à la troisième partie")
    else:
        msgbox("Dommage c'etait l'arme n°3!(la Cross de rink hockey)","Mince","Passer à la troisième partie")
    #je cree la reponse a la verification


    #on passe a la troisieme partie

    msgbox("Voici les lieux suspects où la victime aurait été assassinée \nRetiens-les bien!\n(Tu peux les noter sur un papier.)")
    #je defini la premier partie de ma troisieme partie

    liste2=("1:Dépendance","2:Grenier","3:Salle_de_bains","4:Bureau","5:Chambre","6:Cabane_au_fond_du_jardin")
    msgbox(liste2)
    #je cree la 2eme liste pour les lieus 

    choix3="Est-ce qu'il s'agit d'un endroit chauffé?","Est-ce que c'est dans la maison?","Est-ce que c'est un endroit sombre?","Est-ce un endroit de stockage?"
    #je cree les question pour la troisieme partie

    r=0#r est une varable pour compter

    msga="La reponse est:\nOUI"
    msgb="La reponse est:\nNON"
    #je defini les reponses des questions choix3

    while r<2:#je refais une boucle 

        rep5= buttonbox("Quelles questions veux tu poser?(2 chances en tout)\n","Troisième partie",choices=choix3)
        #je cree l'interface pour poser les questions

        if rep5 == choix3[1] or choix3[0]:
            msgbox(msgb,"","Il te reste peut-être une chance?")
            r=r+1
        elif rep5 == choix3[2] or choix3[3]:
            msgbox(msga,"","Il te reste peut-être une chance?")
            r=r+1
        #il s'agit des reponses aux questions

    rep6=buttonbox("Malheureusement tu n'as plus de chance. \nA ton avis où c'est passé le crime?","Troisième partie",[liste2[0],liste2[1],liste2[2],liste2[3],liste2[4],liste2[5]])
    #c'est l'interface pour savoir si le joueur a trouver le lieu

    if rep6 == liste2[5]:
        msgbox("Bien joué!\nLe troisième indice: TU","Félicitation","Passer à la vérification")
    else:
        msgbox("Dommage c'était le n°6(c'était dans la cabane au fond du jardin)","Mince","Passer à la vérification")
    #ce sont reponse a la verification du lieu


    #on passe a la verification

    repfin=enterbox("Entrer le code grace au 3 indices")
    #ici on ouvre une interface où on peut rentrer du texte

    repfin=repfin.upper()
    #on met la reponse en majuscule pour etre sur que la bonne reponse ne soit pas lu comme fausse

    if repfin == "UBUNTU":
        print("Bravo tu as reussi mon escape game de NSI coef 27! J'espère que ça t'a plu! UBUNTU")
    else:
        print("Ce n'est pas la bonne reponse, c'était UBUNTU! Merci d'avoir joué! UBUNTU")
    #on print la reponse : bon ou pas bon et la fin dans chaque cas

else:
    pass
    #si on met non au ynbox plus haut on est rediriger vers ce else et ca quite le jeu
    