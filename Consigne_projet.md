POUR LE 13 FÉVRIER

vous aller créer un projet autour des thèmes abordés depuis quatre mois : **images, son, texte, données structurées**,
en vous inspirant des mioni-projets qui ont été proposés. Il sera écrit en python et sera PERSONNEL. Une version 
avec GUI sera appréciée. Vous créerez pour le présenter un compte sur Gitlab où seront présents vos fichiers Python,
une documentation au format markdown et une courte vidéo présentant votre projet. Ce projet sera noté coefficient 27.
Seront appréciés sa facilité de mise en œuvre, sa documentation, son organisation, sa rigueur, son originalité,
la touche personnelle que vous pourrez y mettre. Ne vous y mettez pas la veille.

Ce qu'il faut au minimum c'est :
- manipuler des structures de données : listes, ensembles, dictionnaires,...
- créer quelques fonctions pures
- présenter le code avec le schéma du cours en 3 parties
- un seul print
- une documentation avec en particulier un mode d'emploi pour installer et lancer
- un compte gitlab
